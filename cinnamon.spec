%global __requires_exclude ^libcinnamon.so|^libcinnamon-js.so

%global clutter_version 1.12.2
%global cjs_version 3.6.0
%global cinnamon_desktop_version 3.6.0
%global gobject_introspection_version 1.38.0
%global muffin_version 3.6.0
%global json_glib_version 0.13.2

Name:           cinnamon
Version:        3.6.2
Release:        2%{?dist}
Summary:        Window management and application launching for GNOME
License:        GPLv2+ and LGPLv2+
URL:            https://github.com/linuxmint/
Source0:        %url/%{name}/archive/%{version}.tar.gz#/%{name}-%{version}.tar.gz
Source1:        polkit-cinnamon-authentication-agent-1.desktop
Source2:        cinnamon-fedora-25.gschema.override
Source3:        cinnamon-fedora-26.gschema.override
Source4:        cinnamon-fedora-27.gschema.override
Source5:        cinnamon-fedora-28.gschema.override
Source6:        cinnamon-rhel-7.gschema.override

Patch0:         background.patch
Patch1:         autostart.patch
%if 0%{?fedora} >= 26 || 0%{?rhel} >= 8
Patch2:         cinnamon-settings-apps-dnfdragora.patch
%else  # 0%%{?fedora} >= 26 || 0%%{?rhel} >= 8
%if 0%{?fedora}
Patch2:         cinnamon-settings-apps-yumex.patch
%else  # 0%%{?fedora}
Patch2:         cinnamon-settings-apps-rhel-7.patch
%endif # 0%%{?fedora}
%endif # 0%%{?fedora} >= 26 || 0%%{?rhel} >= 8
Patch3:         set_wheel.patch
Patch4:         network-user-connections.patch
Patch5:         revert_25aef37.patch
%if 0%{?fedora} >= 27 || 0%{?rhel} >= 8
Patch6:         default_panal_launcher_tilix.patch
%else
Patch6:         default_panal_launcher.patch
%endif
Patch7:         remove_crap_from_menu.patch
Patch8:         replace-metacity-with-openbox.patch
Patch9:         menu-use-fedora-icon-as-default.patch

BuildRequires:  gcc-c++
BuildRequires:  pkgconfig(clutter-1.0) >= %{clutter_version}
BuildRequires:  pkgconfig(dbus-glib-1)
BuildRequires:  desktop-file-utils
BuildRequires:  pkgconfig(cjs-1.0) >= %{cjs_version}
BuildRequires:  pkgconfig(gconf-2.0)
BuildRequires:  pkgconfig(gl)
BuildRequires:  pkgconfig(gnome-bluetooth-1.0)
BuildRequires:  pkgconfig(libgnome-menu-3.0)
BuildRequires:  pkgconfig(libcinnamon-menu-3.0)
BuildRequires:  pkgconfig(cinnamon-desktop) >= %{cinnamon_desktop_version}
BuildRequires:  gobject-introspection >= %{gobject_introspection_version}
BuildRequires:  pkgconfig(json-glib-1.0) >= %{json_glib_version}
BuildRequires:  pkgconfig(upower-glib)
BuildRequires:  pkgconfig(libnm-glib)
BuildRequires:  pkgconfig(polkit-agent-1)
BuildRequires:  pkgconfig(gudev-1.0)
# for screencast recorder functionality
BuildRequires:  pkgconfig(gstreamer-1.0)
BuildRequires:  intltool
BuildRequires:  pkgconfig(libcanberra)
BuildRequires:  pkgconfig(libcroco-0.6)
BuildRequires:  pkgconfig(gnome-keyring-1)
BuildRequires:  pkgconfig(libsoup-2.4)
# used in unused BigThemeImage
BuildRequires:  pkgconfig(librsvg-2.0)
BuildRequires:  pkgconfig(libmuffin) >= %{muffin_version}
BuildRequires:  pkgconfig(libpulse)
# Bootstrap requirements
BuildRequires:  pkgconfig(gtk-doc)
BuildRequires:  gnome-common
# media keys
BuildRequires:  pkgconfig(libnotify)
BuildRequires:  pkgconfig(lcms2)
BuildRequires:  pkgconfig(colord)
%ifnarch s390 s390x %{?rhel:ppc ppc64}
BuildRequires:  pkgconfig(libwacom)
BuildRequires:  pkgconfig(xorg-wacom)
%endif
BuildRequires:  pkgconfig(xtst)

Requires:       cinnamon-desktop >= %{cinnamon_desktop_version}
Requires:       muffin%{?_isa} >= %{muffin_version}
Requires:       cjs%{?_isa} >= %{cjs_version}
Requires:       gnome-menus%{?_isa} >= 3.0.0-2
# wrapper script used to restart old GNOME session if run --replace
# from the command line
Requires:       gobject-introspection%{?_isa} >= %{gobject_introspection_version}
# needed for loading SVG's via gdk-pixbuf
Requires:       librsvg2%{?_isa}
# needed as it is now split from Clutter
Requires:       json-glib%{?_isa} >= %{json_glib_version}
Requires:       upower%{?_isa}
Requires:       polkit%{?_isa} >= 0.100
# needed for session files
Requires:       cinnamon-session
# needed for schemas
Requires:       at-spi2-atk%{?_isa}
# needed for on-screen keyboard
Requires:       caribou%{?_isa}
# needed for the user menu
Requires:       accountsservice-libs
# needed for settings
%if 0%{?fedora}
Requires:       python2-pexpect
Requires:       python-gobject
%else
Requires:       pexpect
Requires:       pygobject3
%endif
Requires:       dbus-python
Requires:       python-lxml
Requires:       python-pillow
Requires:       PyPAM
Requires:       mintlocale
Requires:       cinnamon-control-center
Requires:       cinnamon-translations
# needed for theme overrides
Requires:       system-logos
%if 0%{?fedora} || 0%{?rhel} >= 7
Requires:       arc-theme
Requires:       google-noto-sans-fonts
Requires:       mint-y-icons
%else
Requires:       mate-themes
%endif
# RequiredComponents in the session files
Requires:       nemo
Requires:       cinnamon-screensaver
# openbox and tint2 are needed for fallback
Requires:       openbox
Requires:       tint2
# required for keyboard applet
Requires:       gucharmap
Requires:       xapps
# required for xlet-settings
Requires:       python3-xapps-overrides%{?_isa}
# required for cinnamon-settings/cinnamon-screensaver-lock-dialog
Requires:       python2-xapps-overrides%{?_isa}
# required for network applet
Requires:       nm-connection-editor
Requires:       network-manager-applet
# required for looking glass
%if 0%{?fedora} || 0%{?rhel} >= 8
Requires:       python2-inotify
%else  # 0%%{?fedora} || 0%%{?rhel} >= 8
Requires:       python-inotify
%endif # 0%%{?fedora} || 0%%{?rhel} >= 8
# required for cinnamon-killer-daemon
Requires:       keybinder3
# required for sound applet
Requires:       wget

Provides:       desktop-notification-daemon


%description
Cinnamon is a Linux desktop which provides advanced
innovative features and a traditional user experience.

The desktop layout is similar to Gnome 2.
The underlying technology is forked from Gnome Shell.
The emphasis is put on making users feel at home and providing
them with an easy to use and comfortable desktop experience.

%package devel-doc
Summary: Development Documentation files for Cinnamon
BuildArch: noarch
Requires: %{name} = %{version}-%{release}

%description devel-doc
This package contains the code documentation for various Cinnamon components.

%prep
%autosetup -p1 -n Cinnamon-%{version}

sed -i -e 's@gksu@pkexec@g' files/usr/bin/cinnamon-settings-users
sed -i -e 's@gnome-orca@orca@g' files/usr/share/cinnamon/cinnamon-settings/modules/cs_accessibility.py
# fix hard coded paths
%ifarch ppc64
sed -i -e 's@/usr/lib/cinnamon-control-center@/usr/lib64/cinnamon-control-center@g' \
 files/usr/share/cinnamon/cinnamon-settings/bin/capi.py
%endif

%if (0%{?rhel} && 0%{?rhel} >= 7)
for f in $(%{__grep} -Rl '#!.*python3') ; do
  %{__sed} -e 's~#!.*python3~#!/usr/bin/python2~g' < ${f} > ${f}.new
  /bin/touch -r ${f}.new ${f}
  mode="$(%{_bindir}/stat -c '%a' ${f})"
  %{__mv} -f ${f}.new ${f}
  %{__chmod} -c ${mode} ${f}
done
%endif # (0%%{?rhel} && 0%%{?rhel} >= 7)

NOCONFIGURE=1 ./autogen.sh

%build
%configure \
 --disable-static \
 --disable-schemas-compile \
 --enable-introspection=yes \
 --enable-compile-warnings=no

%make_build V=1

%install
%make_install

# Remove .la file
rm -rf %{buildroot}%{_libdir}/cinnamon/{libcinnamon-js,libcinnamon}.la

# install schema override
%if 0%{?fedora} >= 28
install -Dpm 0644 %{SOURCE5} %{buildroot}%{_datadir}/glib-2.0/schemas/10_cinnamon-fedora-28.gschema.override
%endif
%if 0%{?fedora} == 27
install -Dpm 0644 %{SOURCE4} %{buildroot}%{_datadir}/glib-2.0/schemas/10_cinnamon-fedora-27.gschema.override
%endif
%if 0%{?fedora} == 26
install -Dpm 0644 %{SOURCE3} %{buildroot}%{_datadir}/glib-2.0/schemas/10_cinnamon-fedora-26.gschema.override
%endif
%if 0%{?fedora} == 25
install -Dpm 0644 %{SOURCE2} %{buildroot}%{_datadir}/glib-2.0/schemas/10_cinnamon-fedora-25.gschema.override
%endif
%if 0%{?rhel} == 7
install -Dpm 0644 %{SOURCE6} %{buildroot}%{_datadir}/glib-2.0/schemas/10_cinnamon-redhat.gschema.override
%endif
# install polkik autostart desktop file
install -Dpm 0644 %{SOURCE1} %{buildroot}%{_datadir}/applications/

desktop-file-validate %{buildroot}%{_datadir}/applications/cinnamon.desktop
desktop-file-validate %{buildroot}%{_datadir}/applications/cinnamon2d.desktop
desktop-file-validate %{buildroot}%{_datadir}/applications/cinnamon-killer-daemon.desktop
desktop-file-validate %{buildroot}%{_datadir}/applications/cinnamon-settings*.desktop
desktop-file-validate %{buildroot}%{_datadir}/applications/cinnamon-menu-editor.desktop
desktop-file-validate %{buildroot}%{_datadir}/applications/polkit-cinnamon-authentication-agent-1.desktop


%post
/bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%if (0%{?rhel} && 0%{?rhel <= 7})
%postun
if [ $1 -eq 0 ] ; then
	/usr/bin/glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :
	/usr/bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
	/usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
/usr/bin/glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :
/usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
%else  # (0%%{?rhel} && 0%%{?rhel <= 7})
%postun
if [ $1 -eq 0 ] ; then
	/usr/bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
	/usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
/usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
%endif # (0%%{?rhel} && 0%%{?rhel <= 7})

%files
%doc README.rst AUTHORS
%license COPYING
%{_bindir}/*
%config(noreplace) %{_sysconfdir}/xdg/menus/*
%{_datadir}/applications/*
%{_datadir}/dbus-1/services/org.Cinnamon.*.service
%{_datadir}/desktop-directories/*
%{_datadir}/glib-2.0/schemas/*
%{_datadir}/cinnamon-session/sessions/*
%{_datadir}/icons/hicolor/*/*/*.svg
%{_datadir}/polkit-1/actions/org.cinnamon.settings-users.policy
%{_datadir}/xsessions/*
%{_datadir}/cinnamon/
%{_libdir}/cinnamon/
%{_libexecdir}/cinnamon/
%{_mandir}/man1/*

%files devel-doc
%doc %{_datadir}/gtk-doc/html/*/

%changelog
* Tue Nov 14 2017 Troy Curtis, Jr <troycurtisjr@gmail.com> - 3.6.2-2
- Point to the new python2/python3-xapps-overrides

* Mon Nov 13 2017 Leigh Scott <leigh123linux@googlemail.com> - 3.6.2-1
- update to 3.6.2 release

* Tue Oct 24 2017 Leigh Scott <leigh123linux@googlemail.com> - 3.6.0-1
- update to 3.6.0 release

* Tue Sep 19 2017 Björn Esser <besser82@fedoraproject.org> - 3.4.6-16
- Update for F27 backgrounds

* Fri Sep 08 2017 Björn Esser <besser82@fedoraproject.org> - 3.4.6-15
- Add online-accounts to cinnamon-settings

* Thu Aug 31 2017 Björn Esser <besser82@fedoraproject.org> - 3.4.6-14
- Fully remove sharing

* Thu Aug 31 2017 Björn Esser <besser82@fedoraproject.org> - 3.4.6-13
- Remove icon for sharing from patches

* Thu Aug 31 2017 Björn Esser <besser82@fedoraproject.org> - 3.4.6-12
- Preserve mode of files when changing hashbang

* Thu Aug 31 2017 Björn Esser <besser82@fedoraproject.org> - 3.4.6-11
- Fix hashbang in regex

* Thu Aug 31 2017 Björn Esser <besser82@fedoraproject.org> - 3.4.6-10
- Use Python 2 on EPEL

* Thu Aug 31 2017 Björn Esser <besser82@fedoraproject.org> - 3.4.6-9
- Small fix for gnome-terminal on EPEL7

* Thu Aug 31 2017 Björn Esser <besser82@fedoraproject.org> - 3.4.6-8
- Revert to gnome-terminal on EPEL7

* Thu Aug 31 2017 Björn Esser <besser82@fedoraproject.org> - 3.4.6-7
- Add patch to adjust cinnamon-settings-apps for EPEL7

* Wed Aug 30 2017 Björn Esser <besser82@fedoraproject.org> - 3.4.6-6
- Adjustments for EPEL7

* Sat Aug 26 2017 Björn Esser <besser82@fedoraproject.org> - 3.4.6-5
- Replace gnome-terminal with tilix in favorites and panel

* Fri Aug 25 2017 Björn Esser <besser82@fedoraproject.org> - 3.4.6-4
- Add gschema override for trilix
- Drop support for fc24
- Add support for fc28

* Thu Aug 10 2017 Leigh Scott <leigh123linux@googlemail.com> - 3.4.6-3
- revert last commit

* Wed Aug 09 2017 Leigh Scott <leigh123linux@googlemail.com> - 3.4.6-2
- rebuild for nemo.desktop name change

* Wed Aug 09 2017 Leigh Scott <leigh123linux@googlemail.com> - 3.4.6-1
- update to 3.4.6 release

* Wed Aug 02 2017 Fedora Release Engineering <releng@fedoraproject.org> - 3.4.4-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Binutils_Mass_Rebuild

* Wed Jul 26 2017 Leigh Scott <leigh123linux@googlemail.com> - 3.4.4-3
- Add build requires mesa-libGL-devel

* Wed Jul 26 2017 Fedora Release Engineering <releng@fedoraproject.org> - 3.4.4-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Thu Jul 06 2017 Leigh Scott <leigh123linux@googlemail.com> - 3.4.4-1
- update to 3.4.4 release

* Wed Jun 21 2017 Leigh Scott <leigh123linux@googlemail.com> - 3.4.2-1
- update to 3.4.2 release

* Wed May 31 2017 Leigh Scott <leigh123linux@googlemail.com> - 3.4.1-2
- Revert 'Remove network-applet changes'

* Tue May 23 2017 Leigh Scott <leigh123linux@googlemail.com> - 3.4.1-1
- update to 3.4.1 release

* Thu May 11 2017 Björn Esser <besser82@fedoraproject.org> - 3.4.0-3
- Updated patch, fixing gir
- Remove network-applet until it works fine with libnm

* Wed May 10 2017 Björn Esser <besser82@fedoraproject.org> - 3.4.0-2
- Add patch to port to libnm-1.2 (rhbz#1413610)

* Thu May 04 2017 Leigh Scott <leigh123linux@googlemail.com> - 3.4.0-1
- update to 3.4.0 release

* Wed Apr 26 2017 Leigh Scott <leigh123linux@googlemail.com> - 3.4.0-0.2.20170426git067e1da
- update to git snapshot

* Thu Apr 20 2017 Leigh Scott <leigh123linux@googlemail.com> - 3.4.0-0.1.20170420git886147d
- update to git snapshot

* Sat Apr 08 2017 Björn Esser <besser82@fedoraproject.org> - 3.2.8-18
- Conditionalize discouraged scriptlets

* Sat Apr 08 2017 Björn Esser <besser82@fedoraproject.org> - 3.2.8-17
- Enable startup-animation by default

* Fri Apr 07 2017 Björn Esser <besser82@fedoraproject.org> - 3.2.8-16
- Use lightdm-settings instead of lightdm-gtk-greeter-settings

* Fri Mar 24 2017 Björn Esser <besser82@fedoraproject.org> - 3.2.8-15
- Make blueberry-applet a weak dependency (rhbz#1429404)
- Change default wallpaper for fc26+
- Add gschema for fc27+

* Sun Feb 19 2017 Björn Esser <besser82@fedoraproject.org> - 3.2.8-14
- Add applet from blueberry by default

* Tue Feb 14 2017 Björn Esser <besser82@fedoraproject.org> - 3.2.8-13
- Require system-logos instead of fedora-logos (rhbz#1421952)

* Sun Feb 05 2017 Björn Esser <besser82@fedoraproject.org> - 3.2.8-12
- Use dnfdragora for package-management in F26+

* Sat Feb 04 2017 Björn Esser <besser82@fedoraproject.org> - 3.2.8-11
- Add new package-managers to favorites

* Fri Feb 03 2017 Björn Esser <besser82@fedoraproject.org> - 3.2.8-10
- Update Patch9 to use a vector-logo instead of a fixed-size png

* Wed Feb 01 2017 Björn Esser <besser82@fedoraproject.org> - 3.2.8-9
- Remove unneded Requires: gnome-python2-gconf

* Wed Feb 01 2017 Björn Esser <besser82@fedoraproject.org> - 3.2.8-8
- Change hard requires on xawtv to a weak one and add
  a comment for which purpose it would be useful

* Tue Jan 31 2017 Björn Esser <besser82@fedoraproject.org> - 3.2.8-7
- Add upstream-patch to fix a segmentation fault that happened in
  sound applet when menu animations were disabled (rhbz#1396110)

* Tue Jan 31 2017 Björn Esser <besser82@fedoraproject.org> - 3.2.8-6
- Adapt Patch9, to not change the value of the default-setting

* Tue Jan 31 2017 Björn Esser <besser82@fedoraproject.org> - 3.2.8-5
- Use 'Noto Sans'-font for screensaver, too

* Mon Jan 30 2017 Björn Esser <besser82@fedoraproject.org> - 3.2.8-4
- Use new theming for F24+ too

* Sun Jan 29 2017 Björn Esser <besser82@fedoraproject.org> - 3.2.8-3
- Add patch to use fedora-icon as default menu-icon

* Sun Jan 29 2017 Björn Esser <besser82@fedoraproject.org> - 3.2.8-2
- Make Cinnamon look more 'Minty' by default
  - Use dark arc-theme with mint-y-icons and google-noto-sans-fonts
    on Fedora 26+ and (possibly) RHEL 8+

* Sat Jan 07 2017 Leigh Scott <leigh123linux@googlemail.com> - 3.2.8-1
- update to 3.2.8 release
- cinnamon-settings: use blueberry instead of blueman
- add build requires gnome-bluetooth-devel

* Sat Dec 31 2016 Björn Esser <bjoern.esser@gmail.com> - 3.2.7-3
- Add upstream patch for tuples in Python-3.6
- Remove hack for python-3.6
- Clean trailing whitspaces
- Drop unused patches

* Thu Dec 29 2016 leigh <leigh123linux@googlemail.com> - 3.2.7-2
- add hack for python-3.6 (rhbz#1408987)

* Thu Dec 22 2016 leigh scott <leigh123linux@googlemail.com> - 3.2.7-1
- update to 3.2.7 release

* Wed Dec 14 2016 leigh scott <leigh123linux@googlemail.com> - 3.2.6-1
- update to 3.2.6 release

* Tue Dec 13 2016 leigh scott <leigh123linux@googlemail.com> - 3.2.5-2
- Patch to mitigate memory leak

* Mon Dec 12 2016 leigh scott <leigh123linux@googlemail.com> - 3.2.5-1
- update to 3.2.5 release

* Sun Dec 11 2016 leigh scott <leigh123linux@googlemail.com> - 3.2.4-1
- update to 3.2.4 release

* Sat Dec 10 2016 leigh scott <leigh123linux@googlemail.com> - 3.2.3-2
- tweak fallback session
- drop mate-panel as tint2 seems a better fit
- replace metacity with openbox as it does
  window decoration and a simple menu

* Sat Dec 10 2016 leigh scott <leigh123linux@googlemail.com> - 3.2.3-1
- update to 3.2.3 release

* Sun Nov 27 2016 leigh scott <leigh123linux@googlemail.com> - 3.2.2-3
- add requires wget (rhbz#1402335)
- use mate-panel for cinnamon fallback (for F25 and up)

* Fri Nov 25 2016 leigh scott <leigh123linux@googlemail.com> - 3.2.2-2
- change default settings for two and three finger click,
  disabling them enables clickpad button areas to function

* Wed Nov 23 2016 leigh scott <leigh123linux@googlemail.com> - 3.2.2-1
- update to 3.2.2 release

* Tue Nov 08 2016 leigh scott <leigh123linux@googlemail.com> - 3.2.1-1
- update to 3.2.1 release

* Mon Nov 07 2016 Leigh Scott <leigh123linux@googlemail.com> - 3.2.0-2
- add requires xapps (for keyboard applet)

* Mon Nov 07 2016 Leigh Scott <leigh123linux@googlemail.com> - 3.2.0-1
- update to 3.2.0 release

* Sun Oct 02 2016 Leigh Scott <leigh123linux@googlemail.com> - 3.0.7-3
- Use F25 background

* Thu Aug 25 2016 Leigh Scott <leigh123linux@googlemail.com> - 3.0.7-2
- add sharing to cinnamon-settings (needs control-center to work)

* Thu Aug 11 2016 Leigh Scott <leigh123linux@googlemail.com> - 3.0.7-1
- update to 3.0.7 release
- set a priority for the gsettings override file
- replace python-opencv with xawtv requires

* Fri Jun 24 2016 Leigh Scott <leigh123linux@googlemail.com> - 3.0.6-1
- update to 3.0.6 release

* Sun Jun 05 2016 Leigh Scott <leigh123linux@googlemail.com> - 3.0.5-2
- Fix missing bluetooth icon

* Tue May 31 2016 Leigh Scott <leigh123linux@googlemail.com> - 3.0.5-1
- update to 3.0.5 release

* Tue May 24 2016 Leigh Scott <leigh123linux@googlemail.com> - 3.0.4-1
- update to 3.0.4 release

* Sun May 22 2016 Leigh Scott <leigh123linux@googlemail.com> - 3.0.3-1
- update to 3.0.3 release

* Tue May 17 2016 Leigh Scott <leigh123linux@googlemail.com> - 3.0.2-2
- add settings for gtk overlay scrollbars and dark theme variant

* Mon May 16 2016 Leigh Scott <leigh123linux@googlemail.com> - 3.0.2-1
- update to 3.0.2 release

* Sun May 01 2016 Leigh Scott <leigh123linux@googlemail.com> - 3.0.1-1
- update to 3.0.1 release

* Sun Apr 24 2016 Leigh Scott <leigh123linux@googlemail.com> - 3.0.0-1
- update to 3.0.0 release

* Mon Apr 04 2016 Leigh Scott <leigh123linux@googlemail.com> - 2.8.8-2
- fix epel requires

* Sat Apr 02 2016 Leigh Scott <leigh123linux@googlemail.com> - 2.8.8-1
- update to 2.8.8 release

* Wed Mar 09 2016 Leigh Scott <leigh123linux@googlemail.com> - 2.8.7-1
- update to 2.8.7 release

* Wed Feb 03 2016 Fedora Release Engineering <releng@fedoraproject.org> - 2.8.6-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Sun Jan 24 2016 Leigh Scott <leigh123linux@googlemail.com> - 2.8.6-2
- Switch to adwaita theme for F24 as mclasen has broken gtk3 again

* Wed Dec 02 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.8.6-1
- update to 2.8.6 release

* Wed Dec 02 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.8.5-3
- fix missing requires for cinnamon-settings

* Sat Nov 21 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.8.5-2
- redo cinnamon-settings-apps patch

* Sat Nov 21 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.8.5-1
- update to 2.8.5 release

* Mon Nov 09 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.8.3-2
- rebuilt

* Mon Nov 09 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.8.3-1
- update to 2.8.3 release

* Sat Oct 31 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.8.2-1
- update to 2.8.2 release

* Mon Oct 26 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.8.1-1
- update to 2.8.1 release

* Thu Oct 22 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.8.0-1
- update to 2.8.0 release

* Wed Sep 09 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.6.13-10
- fix deprecated schema warnings

* Fri Aug 28 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.6.13-9
- add patch to fix glib2 regression

* Tue Aug 25 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.6.13-8
- fix deprecation warning on cinnamon-settings

* Sun Aug 16 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.6.13-7
- try again

* Sun Aug 16 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.6.13-6
- change back to gnome icon theme

* Sun Aug 09 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.6.13-5
- remove .la file and filter requires

* Fri Aug 07 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.6.13-4
- revert upstream flag commit (keyboard applet)

* Wed Jul 29 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.6.13-3
- add f23 schema override

* Mon Jul 20 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.6.13-2
- remove nautilus from menu
- remove other useless crap from menu

* Sun Jul 12 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.6.13-1
- update to 2.6.13 release

* Tue Jul 07 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.6.12-2
- fix gi import versions

* Wed Jul 01 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.6.12-1
- update to 2.6.12 release

* Sat Jun 27 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.6.11-2
- add required cjs version 2.6.2

* Sat Jun 27 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.6.11-1
- update to 2.6.11 release
- spec file clean up

* Sat Jun 20 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.6.8-3
- remove requires blueman

* Wed Jun 17 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.6.8-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Sun Jun 14 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.6.8-1
- update to 2.6.8 release

* Sat Jun 13 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.6.7-4
- remove the bluetooth buildrequires for F19
- make themes work on F20 (due to old gi version)

* Thu Jun 11 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.6.7-3
- tweak override schema

* Sat Jun 06 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.6.7-2
- add requires keybinder3 (bz 1227997)

* Tue Jun 02 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.6.7-1
- update to 2.6.7 release

* Mon Jun 01 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.6.6-1
- update to 2.6.6 release

* Fri May 29 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.6.4-1
- update to 2.6.4 release

* Wed May 27 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.6.3-1
- update to 2.6.3 release

* Sat May 23 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.6.2-2
- Add upstream fixes

* Thu May 21 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.6.2-1
- update to 2.6.2 release

* Thu May 21 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.6.1-2
- add devel-doc subpackage

* Wed May 20 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.6.1-1
- update to 2.6.1 release

* Wed May 20 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.6.0-1
- update to 2.6.0 release

* Fri May 15 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.5.0-0.2.git32284cb
- update to git snapshot

* Tue May 05 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.5.0-0.1.gitc0ea9e7
- update to git snapshot

* Sun Apr 26 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.4.8-1
- update to 2.4.8

* Sat Apr 25 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.4.7-4
- use new gnome-terminal desktop file name

* Sat Apr 25 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.4.7-3
- add br zukitwo-metacity-theme

* Sat Apr 25 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.4.7-2
- tweak schema overrides

* Thu Apr 02 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.4.7-1
- update to 2.4.7

* Tue Feb 10 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.4.6-3
- add requires python-inotify for looking glass

* Sat Jan 24 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.4.6-2
- switch back to zukitwo theme

* Mon Jan 19 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.4.6-1
- update to 2.4.6
- drop upstream patches

* Sun Jan 18 2015 Leigh Scott <leigh123linux@googlemail.com> - 2.4.5-3
- change to nimbus icon theme
- add some upstream fixes

* Tue Dec 09 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.4.5-2
- Fix slideshow for user added pictures (bz 1172008)

* Tue Dec 02 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.4.5-1
- update to 2.4.5
- attempt to fix brightness control detection
- drop requires gstreamer-python

* Wed Nov 26 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.4.4-2
- remove session timeout settings from general

* Sun Nov 23 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.4.4-1
- update to 2.4.4

* Tue Nov 18 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.4.2-2
- add patch to set default panel launchers
- disable the stupid startup-animation effect

* Wed Nov 12 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.4.2-1
- update to 2.4.2

* Sat Nov 08 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.4.1-2
- switch to use xml backgrounds

* Sat Nov 08 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.4.1-1
- update to 2.4.1

* Fri Oct 31 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.4.0-1
- update to 2.4.0

* Fri Oct 10 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.4.0-0.4.gitea748a2
- update to latest git

* Wed Oct 01 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.4.0-0.3.git8304e57
- patch for some of the gtk-3.14 changes

* Tue Sep 30 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.4.0-0.2.git8304e57
- add requires gstreamer-python (needed for c-s 'account details')

* Tue Sep 30 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.4.0-0.1.git8304e57
- update to latest git

* Sat Sep 06 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.2.16-3
- Change theme to Adwaita till the other themes are fixed

* Sat Aug 16 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.2.16-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Wed Aug 13 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.2.16-1
- update to 2.2.16
- drop upstream patches

* Sun Aug 03 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.2.14-9
- fix cinnamon-settings background with python-pillow-2.5

* Sun Aug 03 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.2.14-8
- revert power applet patch (fixed in cinnamon-settings-daemon)

* Tue Jul 29 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.2.14-7
- Cinnamon Settings User: Don't print passwords to stdout

* Tue Jul 22 2014 Kalev Lember <kalevlember@gmail.com> - 2.2.14-6
- Rebuilt for gobject-introspection 1.41.4

* Thu Jul 17 2014 Rex Dieter <rdieter@fedoraproject.org> 2.2.14-5
- rebuild (for pulseaudio, bug #1117683)

* Wed Jul 16 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.2.14-4
- fix power applet with upower-0.99

* Tue Jul 08 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.2.14-3
- backport some menu fixes

* Wed Jul 02 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.2.14-2
- add network applet patch for nm-applet changes

* Fri Jun 27 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.2.14-1
- update to 2.2.14
- Touchpad support: Added support for two and three finger clicks

* Fri Jun 06 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.2.13-5
- modify set_wheel patch

* Tue Jun 03 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.2.13-4
- fix files from being listed twice

* Tue Jun 03 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.2.13-3
- rebuilt

* Fri May 30 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.2.13-2
- add logout sound to schema override

* Mon May 26 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.2.13-1
- update to 2.2.13
- give cinnamon a rpmlint birthday

* Tue May 20 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.2.10-1
- update to 2.2.10

* Sun May 11 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.2.7-1
- update to 2.2.7

* Sat May 10 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.2.6-1
- update to 2.2.6

* Sat May 03 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.2.5-1
- update to 2.2.5
- validate all the cinnamon-settings desktop files

* Fri May 02 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.2.4-1
- update to 2.2.4

* Mon Apr 21 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.2.3-3
- add requires mintlocale

* Tue Apr 15 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.2.3-2
- add requires gucharmap
- add required network packages for network applet
- change to gstreamer1

* Mon Apr 14 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.2.3-1
- update to 2.2.3

* Sat Apr 12 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.2.0-1
- update to 2.2.0

* Wed Apr 02 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.0.14-16
- add patch to disable xinput for cinnamon only (bz 873434)

* Wed Mar 05 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.0.14-15
- Fix desktop file editor

* Thu Feb 20 2014 Kalev Lember <kalevlember@gmail.com> - 2.0.14-14
- Rebuilt for cogl soname bump

* Mon Feb 10 2014 Peter Hutterer <peter.hutterer@redhat.com> - 2.0.14-13
- Rebuild for libevdev soname bump

* Sun Feb 09 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.0.14-12
- cinnamon-settings-users: set wheel instead of sudo

* Fri Feb 07 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.0.14-11
- rebuilt for new cogl .so version

* Wed Jan 22 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.0.14-10
- use autosetup for prep
- trim spec file changelog

* Tue Jan 14 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.0.14-9
- change pexpect requires for epel7

* Tue Jan 14 2014 Leigh Scott <leigh123linux@googlemail.com> - 2.0.14-8
- Add conditionals for epel7
- Add redhat overrides schema file

* Sun Dec 22 2013 Leigh Scott <leigh123linux@googlemail.com> - 2.0.14-7
- Remove bluetooth for F20 as well

* Sun Dec 22 2013 Leigh Scott <leigh123linux@googlemail.com> - 2.0.14-6
- Patch calendar applet for upower changes
- Remove bluetooth

* Sat Dec 07 2013 Leigh Scott <leigh123linux@googlemail.com> - 2.0.14-5
- readd requires python-pexpect for ARM

* Tue Dec 03 2013 Leigh Scott <leigh123linux@googlemail.com> - 2.0.14-4
- add requires gnome-themes

* Mon Dec 02 2013 Leigh Scott <leigh123linux@googlemail.com> - 2.0.14-3
- tweak gschema override again

* Tue Nov 26 2013 Leigh Scott <leigh123linux@googlemail.com> - 2.0.14-2
- add compile fix for F21

* Tue Nov 26 2013 Leigh Scott <leigh123linux@googlemail.com> - 2.0.14-1
- update to 2.0.14
- remove conflicts wallpapoz (bz 1029554)
- remove nm-applet from autostart (bz 1034887)

* Sun Nov 24 2013 Leigh Scott <leigh123linux@googlemail.com> - 2.0.13-3
- patch to restore panel icon bounce

* Sun Nov 24 2013 Leigh Scott <leigh123linux@googlemail.com> - 2.0.13-2
- set default theme to zukitwo

* Sun Nov 24 2013 Leigh Scott <leigh123linux@googlemail.com> - 2.0.13-1
- update to 2.0.13
- tweak gschema override again

* Thu Nov 14 2013 Leigh Scott <leigh123linux@googlemail.com> - 2.0.12-2
- add conflicts wallpapoz (bz 1029554)

* Mon Nov 11 2013 Leigh Scott <leigh123linux@googlemail.com> - 2.0.12-1
- update to 2.0.12
- tweak gschema override again

* Sun Nov 10 2013 Leigh Scott <leigh123linux@googlemail.com> - 2.0.11-1
- update to 2.0.11
- remove upstream patch
- tweak gschema override

* Tue Nov 05 2013 Leigh Scott <leigh123linux@googlemail.com> - 2.0.10-2
- add patch to draw desktop background immediately

* Sun Nov 03 2013 Leigh Scott <leigh123linux@googlemail.com> - 2.0.10-1
-  update to 2.0.10

* Fri Nov 01 2013 Leigh Scott <leigh123linux@googlemail.com> - 2.0.8-1
- update to 2.0.8
- add autostart file for polkit

* Wed Oct 30 2013 Leigh Scott <leigh123linux@googlemail.com> - 2.0.7-1
- update to 2.0.7

* Fri Oct 25 2013 Leigh Scott <leigh123linux@googlemail.com> - 2.0.6-1
- update to 2.0.6

* Thu Oct 24 2013 Leigh Scott <leigh123linux@googlemail.com> - 2.0.5-1
- update to 2.0.5

* Fri Oct 18 2013 Leigh Scott <leigh123linux@googlemail.com> - 2.0.3-1
- update to 2.0.3

* Thu Oct 17 2013 Leigh Scott <leigh123linux@googlemail.com> - 2.0.2-3
- add custom nm-applet file as the stock one is set to NotShowIn Gnome

* Thu Oct 10 2013 Leigh Scott <leigh123linux@googlemail.com> - 2.0.2-2
- add policy file for "users and groups" setting
- add nm-applet to required components
- add upstream commits

* Wed Oct 09 2013 Leigh Scott <leigh123linux@googlemail.com> - 2.0.2-1
- update to 2.0.2
- drop upstream patch

* Mon Oct 07 2013 Leigh Scott <leigh123linux@googlemail.com> - 1.9.2-4
- revert ST changes

* Mon Oct 07 2013 Leigh Scott <leigh123linux@googlemail.com> - 1.9.2-3
- patch with upstream commits for
- lightdm and a
- ST crash on user switching

* Tue Oct 01 2013 Leigh Scott <leigh123linux@googlemail.com> - 1.9.2-2
- set some sound defaults

* Mon Sep 30 2013 Leigh Scott <leigh123linux@googlemail.com> - 1.9.2-1
- update to 1.9.2

* Tue Sep 24 2013 Leigh Scott <leigh123linux@googlemail.com> - 1.9.2-0.27.git8a53cfb
- use the right conditional (too much beer)

* Tue Sep 24 2013 Leigh Scott <leigh123linux@googlemail.com> - 1.9.2-0.26.git8a53cfb
- re-add bluetooth support for F19

* Tue Sep 24 2013 Leigh Scott <leigh123linux@googlemail.com> - 1.9.2-0.25.git8a53cfb
- Remove ExcludeArch for ARM
- remove the python-pexpect requires for ARM

* Sat Sep 21 2013 Leigh Scott <leigh123linux@googlemail.com> - 1.9.2-0.24.git8a53cfb
- patch to add input-source switching keybindings

* Thu Sep 19 2013 Leigh Scott <leigh123linux@googlemail.com> - 1.9.2-0.23.git8a53cfb
- patch keyboard applet (also fixes input-switching)

* Wed Sep 18 2013 Leigh Scott <leigh123linux@googlemail.com> - 1.9.2-0.22.git8a53cfb
- update to latest git

* Wed Sep 11 2013 Leigh Scott <leigh123linux@googlemail.com> - 1.9.2-0.21.git2d1ac4d
- ExcludeArch for ARM due to missing dep (python-pexpect)

* Sun Aug 25 2013 Leigh Scott <leigh123linux@googlemail.com> - 1.9.2-0.20.git2d1ac4d
- update to latest git
- Change buildrequires to cinnamon-desktop-devel

* Sat Aug 24 2013 Leigh Scott <leigh123linux@googlemail.com> - 1.9.2-0.19.gitd4305ab
- add requires cinnamon-translations

* Fri Aug 23 2013 Leigh Scott <leigh123linux@googlemail.com> - 1.9.2-0.18.gitd4305ab
- update to latest git
- adjust for new cinnamon-translations package

* Thu Aug 22 2013 Leigh Scott <leigh123linux@googlemail.com> - 1.9.2-0.17.git8bdd61f
- rebuilt

* Tue Aug 20 2013 Leigh Scott <leigh123linux@googlemail.com> - 1.9.2-0.16.git8bdd61f
- update to latest git
- drop upstream patches

* Sat Aug 10 2013 Leigh Scott <leigh123linux@googlemail.com> - 1.9.2-0.15.git19b4b43
- redo gsettings patch

* Sat Aug 10 2013 Leigh Scott <leigh123linux@googlemail.com> - 1.9.2-0.14.git19b4b43
- update to latest git
- drop upstream fixes

* Sat Aug 10 2013 Leigh Scott <leigh123linux@googlemail.com> - 1.9.2-0.13.gita1dd2a1
- add patch to remove obsolete gsettings for menu and button icon till upstream fixes it

* Fri Aug 09 2013 Kalev Lember <kalevlember@gmail.com> - 1.9.2-0.12.gita1dd2a1
- Rebuilt for cogl 1.15.4 soname bump

* Mon Jul 29 2013 leigh <leigh123linux@googlemail.com> - 1.9.2-0.11.gita1dd2a1
- fix bluetooth patch again

* Mon Jul 29 2013 Leigh Scott <leigh123linux@googlemail.com> - 1.9.2-0.10.gita1dd2a1
- remove some fixes and upstream patches
- redo bluetooth patch

* Sun Jul 28 2013 Leigh Scott <leigh123linux@googlemail.com> - 1.9.2-0.9.git39fc3a7
- patch to use cinnamon-control-center bluetooth

* Sun Jul 28 2013 Leigh Scott <leigh123linux@googlemail.com> - 1.9.2-0.8.git39fc3a7
- add virtual provides desktop-notification-daemon
- fix missing settings-users menu icon

* Sat Jul 27 2013 Leigh Scott <leigh123linux@googlemail.com> - 1.9.2-0.7.git39fc3a7
- fix icon path for user and groups

* Fri Jul 26 2013 Leigh Scott <leigh123linux@googlemail.com> - 1.9.2-0.6.git39fc3a7
- drop screensaver patch

* Fri Jul 26 2013 Leigh Scott <leigh123linux@googlemail.com> - 1.9.2-0.5.git39fc3a7
- update to latest git
- fix panel-edit crash

* Thu Jul 25 2013 Leigh Scott <leigh123linux@googlemail.com> - 1.9.2-0.4.git435e7a2
- Fix automake warnings

* Thu Jul 25 2013 Leigh Scott <leigh123linux@googlemail.com> - 1.9.2-0.3.git435e7a2
- update to latest git
- fix default theme
- use fedora firewall in settings
- use beesu for user accounts instead of gksu

* Wed Jul 24 2013 Leigh Scott <leigh123linux@googlemail.com> - 1.9.2-0.2.gitc321232
- fix control-center settings on x86_64
- drop clutter xinput patch
- add missing requirements for cinnamon-settings
- redo screensaver patch to enable/disable lock password

* Tue Jul 23 2013 Leigh Scott <leigh123linux@googlemail.com> - 1.9.2-0.1.gitc321232
- rebase for cinnamon next

* Tue Jul 23 2013 Leigh Scott <leigh123linux@googlemail.com> - 1.9.1-19
- fix permissions on cinnamon3d

* Mon Jul 22 2013 Leigh Scott <leigh123linux@googlemail.com> - 1.9.1-18
- fix permissions on cinnamon-launcher-creator

* Tue Jul 16 2013 leigh scott <leigh123linux@googlemail.com> - 1.9.1-17
- add patch to fix cinnamon-menu-editor

* Fri Jul 12 2013 leigh scott <leigh123linux@googlemail.com> - 1.9.1-16
- remove GC from cinnamon-global.c

* Wed Jul 10 2013 leigh scott <leigh123linux@googlemail.com> - 1.9.1-15
- fix input-source-switcher autostart

* Wed Jul 10 2013 leigh scott <leigh123linux@googlemail.com> - 1.9.1-14
- fix input-source-switcher

* Fri Jun 14 2013 leigh scott <leigh123linux@googlemail.com> - 1.9.1-13
- spec file clean up

* Thu Jun 13 2013 leigh scott <leigh123linux@googlemail.com> - 1.9.1-12
- Fix automount

* Thu Jun 13 2013 Dan Horák <dan[at]danny.cz> - 1.9.1-11
- fix build on s390(x) - no wacom there

* Wed Jun 12 2013 leigh scott <leigh123linux@googlemail.com> - 1.9.1-10
- fix the screensaver tab in cinnamon-settings

* Sun Jun 09 2013 leigh scott <leigh123linux@googlemail.com> - 1.9.1-9
- add requires gnome-screensaver

* Sun Jun 09 2013 leigh scott <leigh123linux@googlemail.com> - 1.9.1-8
- Fix media keys

* Thu Jun 06 2013 leigh scott <leigh123linux@googlemail.com> - 1.9.1-7
- change how the screen lock autostarts

* Thu Jun 06 2013 leigh scott <leigh123linux@googlemail.com> - 1.9.1-6
- add requires nemo

* Thu Jun 06 2013 leigh scott <leigh123linux@googlemail.com> - 1.9.1-5
- autostart nemo differently so we dont squash nautilus

* Thu Jun 06 2013 leigh scott <leigh123linux@googlemail.com> - 1.9.1-4
- Patch so screen lock uses gnome-screensaver
- add gnome-screensaver autostart files
- add patch to remove obex file transfer

* Tue Jun 04 2013 Leigh Scott <leigh123linux@googlemail.com> - 1.9.1-3
- add cinnamon-fedora.gschema.override file

* Tue Jun 04 2013 leigh scott <leigh123linux@googlemail.com> - 1.9.1-2
- patch for mozjs-17 changes

* Sat Jun 01 2013 Leigh Scott <leigh123linux@googlemail.com> - 1.9.1-1
- Update to version 1.9.1

* Sat Jun 01 2013 Leigh Scott <leigh123linux@googlemail.com> - 1.8.7-2
- Re-add build requires versions

